﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microservice.DBContexts;
using Microservice.Repository;
using Swashbuckle.AspNetCore.Swagger;

namespace Microservice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<ProductContext>(o => o.UseSqlServer(Configuration.GetConnectionString("MicroserviceDB")));
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1",
                                   new Info
                                   {
                                       Title = "Microservice API",
                                       Version = "v1",
                                       Description = "This documentation provide the information about api endpoints",
                                       Contact = new Contact()
                                       {
                                           Name = "Samuel Avilés",
                                           Url = "www.samuelmicroservice.mx"
                                       }
                                   });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "/");
            });
        }
    }
}

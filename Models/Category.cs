﻿namespace Microservice.Models
{
    /// <summary>
    /// Class with properties for category
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Category identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Category name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Category description
        /// </summary>
        public string Description { get; set; }
    }
}

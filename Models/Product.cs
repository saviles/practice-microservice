﻿namespace Microservice.Models
{
    /// <summary>
    /// Class with properties for product
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Product name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Product description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Price of the product
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Product category
        /// </summary>
        public int CategoryId { get; set; }
    }
}
